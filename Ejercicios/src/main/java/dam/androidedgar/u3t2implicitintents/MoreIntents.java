package dam.androidedgar.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.actions.NoteIntents;

public class MoreIntents extends AppCompatActivity implements View.OnClickListener{
    public static final String IMPLICIT_INTENTS = "ImplicitIntents";
    private EditText etTelefono, etTitulo, etContenido;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moreintents);

        setUI();
    }

    private void setUI(){
        Button btWifi, btTelefono, btAlarma;

        btWifi = findViewById(R.id.btWifi);
        etTelefono = findViewById(R.id.etTelefono);
        etTitulo = findViewById(R.id.etTitulo);
        etContenido = findViewById(R.id.etContenido);
        btTelefono = findViewById(R.id.btTelefono);
        btAlarma = findViewById(R.id.btNota);

        btAlarma.setOnClickListener(this);
        btWifi.setOnClickListener(this);
        btTelefono.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btWifi:
                openWifi();
                break;
            case R.id.btTelefono:
                llamar(etTelefono.getText().toString());
                break;
            case R.id.btNota:
                createNote(etTitulo.getText().toString(), etContenido.getText().toString());
                break;
        }
    }


    private void openWifi () {
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void llamar(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void createNote(String subject, String text) {
        Intent intent = new Intent(NoteIntents.ACTION_CREATE_NOTE)
                .putExtra(NoteIntents.EXTRA_NAME, subject)
                .putExtra(NoteIntents.EXTRA_TEXT, text);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }




}